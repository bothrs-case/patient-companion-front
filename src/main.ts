import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import VueRx from "vue-rx";
import { TipsService } from "@/services/tips.service";
import "./../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { UserService } from "@/services/user.service";
import { TopicsService } from "@/services/topics.service";

Vue.config.productionTip = false;

Vue.use(VueRx);

new Vue({
  router,
  provide: () => ({
    tipsService: new TipsService(),
    userService: new UserService(),
    topicsService: new TopicsService()
  }),
  render: h => h(App)
}).$mount("#app");
