import {from, Observable, of} from "rxjs";
import { Tip } from "@/models/tip";
import axios from "axios";

export class TipsService {
  /*getTips(): Observable<Tip[]> {
    return of([
      {
        name: "Tip of the day",
        title: "Swallowing",
        intro:
          "If you struggle with swallowing, eat soft foods wherever possible. You can check out some of our soft food recipes for ideas here.",
        text: "Dit is text",
        image: "assets/tip.png",
        createdTimestamp: new Date()
      }
    ]);
  }*/

  getTipOfTheDay(): Observable<Tip> {
    return from(axios.get<Tip>('http://localhost:3000/api/tips/tip-of-the-day')
        .then(({data}) => data));
  }
}
