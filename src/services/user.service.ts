import { Observable, of } from "rxjs";

export class UserService {
  getUser(): Observable<User> {
    return of({
      name: "Stef"
    });
  }
}
