import {from, Observable, of} from "rxjs";
import { Topic } from "@/models/topic";
import axios from "axios";

export class TopicsService {
  getTopicOfTheDay(): Observable<Topic> {
    return from(axios.get<Topic>('http://localhost:3000/api/topics/this-week')
        .then(({ data }) => data));
  }
}
