export interface Post {
  title: string;
  text: string;
  author: string;
  createdTimestamp: Date;
}
