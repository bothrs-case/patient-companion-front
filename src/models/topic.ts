import { Post } from "@/models/post";

export interface Topic {
  name: string;
  posts: Post[];
}
