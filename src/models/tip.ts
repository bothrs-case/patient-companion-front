export interface Tip {
  name: string;
  title: string;
  intro: string;
  text: string;
  image: string;
  createdTimestamp: Date;
}
